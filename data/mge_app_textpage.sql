-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 72.10.33.143    Database: mge
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_textpage`
--

DROP TABLE IF EXISTS `app_textpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_textpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lead` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `highlight` tinyint(1) NOT NULL,
  `display_date` datetime NOT NULL,
  `small_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `large_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` date NOT NULL,
  `updated` datetime NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `discr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `search_idx` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_textpage`
--

LOCK TABLES `app_textpage` WRITE;
/*!40000 ALTER TABLE `app_textpage` DISABLE KEYS */;
INSERT INTO `app_textpage` VALUES (1,'Service 1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>',NULL,1,'2012-01-01 00:00:00','590dc4773d8aa.png','590dc4773da64.png','590dc4773d678.png','2017-05-06','2017-05-06 14:41:27','service_1','service'),(2,'Project 1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>',0,1,'2012-01-01 00:00:00','590dc4a201cd9.png','590dc4a201e9a.png','590dc4a201a73.png','2017-05-06','2017-05-06 21:45:19','project_1','project'),(3,'Blogpost 1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>',NULL,1,'2012-01-01 00:00:00','590dc4d6dbda1.png','590dc4d6dbf57.png','590dc4d6dbb16.png','2017-05-06','2017-05-06 14:43:02','blogpost_1','blogpost'),(4,'News 1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>',NULL,1,'2012-01-01 00:00:00','590dc58241978.png','590dc58241b1d.png','590dc58241729.png','2017-05-06','2017-05-06 14:45:54','news_1','article'),(5,'Project 2','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>',0,1,'2012-01-01 00:00:00','590e23bfddf37.png','590e23bfde103.png','590e23bfddcbf.png','2017-05-06','2017-05-06 21:27:59','project_2','project'),(6,'Tesz újdonság','<p>Ide j&ouml;n egy r&ouml;videbb sz&ouml;veg, ami a főoldali dobozban fog csak l&aacute;tsz&oacute;dni.</p>','<p>Ez a sz&ouml;veg lesz az oldal tartalma, ha a l&aacute;togat&oacute; megnyitja ezt a h&iacute;rt.</p>',NULL,1,'2012-01-01 00:00:00','590e2e01ca3fa.png','590e2e01ca5ea.png','590e2e01ca1c7.png','2017-05-06','2017-05-06 22:11:45','tesz_ujdonsag','article'),(7,'blabla blog','<p>Betekintő blog</p>','<p>Teljes blogbejegyz&eacute;s</p>',NULL,1,'2012-01-01 00:00:00','590e2eae69f5d.png','590e2eae6a1ea.png','590e2eae69cd7.png','2017-05-06','2017-05-06 22:14:38','blabla_blog','blogpost');
/*!40000 ALTER TABLE `app_textpage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 22:52:33
