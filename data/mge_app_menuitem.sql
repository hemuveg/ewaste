-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 72.10.33.143    Database: mge
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_menuitem`
--

DROP TABLE IF EXISTS `app_menuitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_menuitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `created` date NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C251927B79066886` (`root_id`),
  KEY `IDX_C251927B727ACA70` (`parent_id`),
  KEY `IDX_C251927BAFC2B591` (`module_id`),
  CONSTRAINT `FK_C251927BAFC2B591` FOREIGN KEY (`module_id`) REFERENCES `app_module` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C251927B727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `app_menuitem` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C251927B79066886` FOREIGN KEY (`root_id`) REFERENCES `app_menuitem` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_menuitem`
--

LOCK TABLES `app_menuitem` WRITE;
/*!40000 ALTER TABLE `app_menuitem` DISABLE KEYS */;
INSERT INTO `app_menuitem` VALUES (1,1,NULL,NULL,'Root','',1,16,0,'2017-05-06','2017-05-06 20:30:58'),(7,1,1,6,'News','news',4,5,1,'2017-05-06','2017-05-06 21:29:43'),(8,1,1,4,'About us','about_us',8,11,1,'2017-05-06','2017-05-06 21:19:41'),(9,1,8,4,'Team','team',9,10,2,'2017-05-06','2017-05-06 21:19:51'),(10,1,1,2,'Projects','projects',12,13,1,'2017-05-06','2017-05-06 21:20:13'),(11,1,1,3,'Services','services',14,15,1,'2017-05-06','2017-05-06 21:20:22'),(12,1,1,5,'Homepage','homepage',2,3,1,'2017-05-06','2017-05-06 21:28:59'),(13,1,1,1,'Blog','blog',6,7,1,'2017-05-06','2017-05-06 21:30:35');
/*!40000 ALTER TABLE `app_menuitem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 22:52:24
