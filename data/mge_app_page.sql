-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 72.10.33.143    Database: mge
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_page`
--

DROP TABLE IF EXISTS `app_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` date NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_11249380381BED05` (`menuitem_id`),
  CONSTRAINT `FK_11249380381BED05` FOREIGN KEY (`menuitem_id`) REFERENCES `app_menuitem` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_page`
--

LOCK TABLES `app_page` WRITE;
/*!40000 ALTER TABLE `app_page` DISABLE KEYS */;
INSERT INTO `app_page` VALUES (1,8,'About us','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','2017-05-06','2017-05-06 21:20:59'),(2,9,'Team','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>\r\n\r\n<p>Ez lenne itt a csapat aloldal.</p>','2017-05-06','2017-05-06 22:19:00');
/*!40000 ALTER TABLE `app_page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 22:52:13
