-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 72.10.33.143    Database: mge
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_textcontent`
--

DROP TABLE IF EXISTS `app_textcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_textcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `display_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_textcontent`
--

LOCK TABLES `app_textcontent` WRITE;
/*!40000 ALTER TABLE `app_textcontent` DISABLE KEYS */;
INSERT INTO `app_textcontent` VALUES (1,'mainpage_phone','<p>00 36 20 900 0000</p>','2012-01-01 00:00:00'),(2,'mainpage_address','<p>CH-3000 Bern</p>','2012-01-01 00:00:00'),(3,'mainpage_about_us','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','2012-01-01 00:00:00'),(4,'mainpage_why_us','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id nunc in est consectetur rhoncus nec eget nulla. Vivamus id arcu eu arcu viverra semper nec ac risus. Nulla at pretium sem. Curabitur lacinia turpis sapien, vitae tincidunt magna vestibulum in. Mauris accumsan, nunc id ullamcorper accumsan, ante dui varius ex, sodales tempus augue risus et sapien. Quisque porttitor tellus nec ex fermentum malesuada. Maecenas porttitor facilisis nibh sit amet tempor. Ut non metus sit amet augue volutpat bibendum nec non enim.</p>','2012-01-01 00:00:00');
/*!40000 ALTER TABLE `app_textcontent` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 22:52:19
