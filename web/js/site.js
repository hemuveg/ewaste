var system_folder = 'http://livedemo00.template-help.com/wordpress_53984/wp-content/themes/CherryFramework/admin/data_management/',
        CHILD_URL = 'http://livedemo00.template-help.com/wordpress_53984/wp-content/themes/theme53984',
        PARENT_URL = 'http://livedemo00.template-help.com/wordpress_53984/wp-content/themes/CherryFramework',
        CURRENT_THEME = 'theme53984'
        ;
jQuery(function () {
  jQuery('.sf-menu').mobileMenu({defaultText: "Navigate to..."});
});
// Init navigation menu
jQuery(function () {
  // main navigation init
  jQuery('ul.sf-menu').superfish({
    delay: 1000, // the delay in milliseconds that the mouse can remain outside a sub-menu without it closing
    animation: {
      opacity: "show",
      height: "show"
    }, // used to animate the sub-menu open
    speed: "normal", // animation speed
    autoArrows: false, // generation of arrow mark-up (for submenu)
    disableHI: true // to disable hoverIntent detection
  });
  //Zoom fix
  //IPad/IPhone
  var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
          ua = navigator.userAgent,
          gestureStart = function () {
            viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
          },
          scaleFix = function () {
            if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
              viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
              document.addEventListener("gesturestart", gestureStart, false);
            }
          };
  scaleFix();
});
jQuery(document).ready(function () {
  if (!device.mobile() && !device.tablet()) {
    jQuery('.nav__primary').tmStickUp({
      correctionSelector: jQuery('#wpadminbar')
      , listenSelector: jQuery('.listenSelector')
      , active: true, pseudo: true});
  }
});
jQuery(document).ready(function ($) {
  if (!device.mobile() && !device.tablet()) {
    liteModeSwitcher = false;
  } else {
    liteModeSwitcher = true;
  }
  if ($.browser.msie && parseInt($.browser.version) < 9) {
    liteModeSwitcher = true;
  }
  if ( jQuery('#parallax-slider-570e0b6b2f3c3').length ) {
    jQuery('#parallax-slider-570e0b6b2f3c3').parallaxSlider({
      parallaxEffect: "parallax_effect_normal"
      , parallaxInvert: false, animateLayout: "simple-fade-eff"
      , duration: 1500, autoSwitcher: true, autoSwitcherDelay: 10000, scrolling_description: false, slider_navs: true, slider_pagination: "none_pagination"
      , liteMode: liteModeSwitcher
    });
  }
});
jQuery(document).ready(function ($) {
  if ( $("#isotopeview-shortcode-570e0b6b80e41").length ) {
    $("#isotopeview-shortcode-570e0b6b80e41").cherryIsotopeView({
      columns: 3,
      fullwidth: true,
      layout: "masonry"
    });
  }
});

/* <![CDATA[ */
var items_custom = [[0, 1], [480, 2], [768, 3], [980, 4], [1170, 5]];
/* ]]> */
deleteCookie('cf-cookie-banner');
