<?php

namespace AppBundle;

use AppBundle\DependencyInjection\Extension\AppExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
  public function getContainerExtension()
  {
    return new AppExtension();
  }
}
