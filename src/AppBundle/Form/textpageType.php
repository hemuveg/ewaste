<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
//use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class textpageType extends AbstractType
{

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'class' => 'AppBundle\Entity\Menuitem',
      'choice_label' => 'getTitle',
      'query_builder' => function (EntityRepository $er)
      {
        return $er->createQueryBuilder('m')
//          ->leftJoin('AppBundle\Entity\Page', 'p', Join::WITH, 'm.id = p.menuitem')
          ->andWhere('m.module = :module')
            ->setParameter('module', 6)
//          ->andWhere('p.id IS NULL')
          ->orderBy('m.lft', 'ASC');
      },
    ));
  }
  /**
   * {@inheritdoc}
   */
  public function getParent()
  {
    return 'Symfony\Bridge\Doctrine\Form\Type\EntityType';
  }

}
