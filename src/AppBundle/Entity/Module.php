<?php

namespace AppBundle\Entity;

/**
 * Module
 */
class Module
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $menuitems;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->menuitems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Module
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add menuitem
     *
     * @param \AppBundle\Entity\Menuitem $menuitem
     *
     * @return Module
     */
    public function addMenuitem(\AppBundle\Entity\Menuitem $menuitem)
    {
        $this->menuitems[] = $menuitem;

        return $this;
    }

    /**
     * Remove menuitem
     *
     * @param \AppBundle\Entity\Menuitem $menuitem
     */
    public function removeMenuitem(\AppBundle\Entity\Menuitem $menuitem)
    {
        $this->menuitems->removeElement($menuitem);
    }

    /**
     * Get menuitems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMenuitems()
    {
        return $this->menuitems;
    }

    public function __toString()
    {
      return $this->title;
    }
}
