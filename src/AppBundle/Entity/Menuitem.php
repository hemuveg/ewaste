<?php

namespace AppBundle\Entity;

/**
 * Menuitem
 */
class Menuitem
{

  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $title;

  /**
   * @var string
   */
  private $slug;

  /**
   * @var integer
   */
  private $root_id;

  /**
   * @var integer
   */
  private $parent_id;

  /**
   * @var integer
   */
  private $lft;

  /**
   * @var integer
   */
  private $rgt;

  /**
   * @var integer
   */
  private $lvl;

  /**
   * @var \DateTime
   */
  private $created;

  /**
   * @var \DateTime
   */
  private $updated;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $children;

  /**
   * @var \AppBundle\Entity\Menuitem
   */
  private $root;

  /**
   * @var \AppBundle\Entity\Menuitem
   */
  private $parent;

  /**
   * @var \AppBundle\Entity\Module
   */
  private $module;

  /**
   * @var \AppBundle\Entity\Page
   */
  private $page;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->children = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   *
   * @return Menuitem
   */
  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  /**
   * Get title
   *
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Set slug
   *
   * @param string $slug
   *
   * @return Menuitem
   */
  public function setSlug($slug)
  {
    $this->slug = $slug;

    return $this;
  }

  /**
   * Get slug
   *
   * @return string
   */
  public function getSlug()
  {
    return $this->slug;
  }

  /**
   * Set rootId
   *
   * @param integer $rootId
   *
   * @return Menuitem
   */
  public function setRootId($rootId)
  {
    $this->root_id = $rootId;

    return $this;
  }

  /**
   * Get rootId
   *
   * @return integer
   */
  public function getRootId()
  {
    return $this->root_id;
  }

  /**
   * Set parentId
   *
   * @param integer $parentId
   *
   * @return Menuitem
   */
  public function setParentId($parentId)
  {
    $this->parent_id = $parentId;

    return $this;
  }

  /**
   * Get parentId
   *
   * @return integer
   */
  public function getParentId()
  {
    return $this->parent_id;
  }

  /**
   * Set lft
   *
   * @param integer $lft
   *
   * @return Menuitem
   */
  public function setLft($lft)
  {
    $this->lft = $lft;

    return $this;
  }

  /**
   * Get lft
   *
   * @return integer
   */
  public function getLft()
  {
    return $this->lft;
  }

  /**
   * Set rgt
   *
   * @param integer $rgt
   *
   * @return Menuitem
   */
  public function setRgt($rgt)
  {
    $this->rgt = $rgt;

    return $this;
  }

  /**
   * Get rgt
   *
   * @return integer
   */
  public function getRgt()
  {
    return $this->rgt;
  }

  /**
   * Set lvl
   *
   * @param integer $lvl
   *
   * @return Menuitem
   */
  public function setLvl($lvl)
  {
    $this->lvl = $lvl;

    return $this;
  }

  /**
   * Get lvl
   *
   * @return integer
   */
  public function getLvl()
  {
    return $this->lvl;
  }

  /**
   * Set created
   *
   * @param \DateTime $created
   *
   * @return Menuitem
   */
  public function setCreated($created)
  {
    $this->created = $created;

    return $this;
  }

  /**
   * Get created
   *
   * @return \DateTime
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set updated
   *
   * @param \DateTime $updated
   *
   * @return Menuitem
   */
  public function setUpdated($updated)
  {
    $this->updated = $updated;

    return $this;
  }

  /**
   * Get updated
   *
   * @return \DateTime
   */
  public function getUpdated()
  {
    return $this->updated;
  }

  /**
   * Add child
   *
   * @param \AppBundle\Entity\Menuitem $child
   *
   * @return Menuitem
   */
  public function addChild(\AppBundle\Entity\Menuitem $child)
  {
    $this->children[] = $child;

    return $this;
  }

  /**
   * Remove child
   *
   * @param \AppBundle\Entity\Menuitem $child
   */
  public function removeChild(\AppBundle\Entity\Menuitem $child)
  {
    $this->children->removeElement($child);
  }

  /**
   * Get children
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getChildren()
  {
    return $this->children;
  }

  /**
   * Set root
   *
   * @param \AppBundle\Entity\Menuitem $root
   *
   * @return Menuitem
   */
  public function setRoot(\AppBundle\Entity\Menuitem $root = null)
  {
    $this->root = $root;

    return $this;
  }

  /**
   * Get root
   *
   * @return \AppBundle\Entity\Menuitem
   */
  public function getRoot()
  {
    return $this->root;
  }

  /**
   * Set parent
   *
   * @param \AppBundle\Entity\Menuitem $parent
   *
   * @return Menuitem
   */
  public function setParent(\AppBundle\Entity\Menuitem $parent = null)
  {
    $this->parent = $parent;

    return $this;
  }

  /**
   * Get parent
   *
   * @return \AppBundle\Entity\Menuitem
   */
  public function getParent()
  {
    return $this->parent;
  }

  /**
   * Set module
   *
   * @param \AppBundle\Entity\Module $module
   *
   * @return Menuitem
   */
  public function setModule(\AppBundle\Entity\Module $module = null)
  {
    $this->module = $module;

    return $this;
  }

  /**
   * Get module
   *
   * @return \AppBundle\Entity\Module
   */
  public function getModule()
  {
    return $this->module;
  }

  public function getPosition()
  {
    return str_repeat('&nbsp;', $this->lvl * 3) . $this->title;
  }

  public function getPage()
  {
    return $this->page;
  }

  public function setPage(\AppBundle\Entity\Page $page)
  {
    $this->page = $page;
    return $this;
  }

  public function __toString()
  {
    return $this->title;
  }

}
