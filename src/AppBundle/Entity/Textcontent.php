<?php

namespace AppBundle\Entity;

/**
 * Textcontent
 */
class Textcontent
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $zone;

    /**
     * @var string
     */
    private $content;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zone
     *
     * @param string $zone
     *
     * @return Textcontent
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Textcontent
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * @var \DateTime
     */
    private $display_date;


    /**
     * Set displayDate
     *
     * @param \DateTime $displayDate
     *
     * @return Textcontent
     */
    public function setDisplayDate($displayDate)
    {
        $this->display_date = $displayDate;

        return $this;
    }

    /**
     * Get displayDate
     *
     * @return \DateTime
     */
    public function getDisplayDate()
    {
        return $this->display_date;
    }
}
