<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * Textpage
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
abstract class Textpage
{

  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $title;

  /**
   * @var string
   */
  private $lead;

  /**
   * @var string
   */
  private $content;

  /**
   * @var integer
   */
  private $status;

  /**
   * @var boolean
   */
  private $highlight;

  /**
   * @var \DateTime
   */
  private $display_date;

  /**
   * @var \DateTime
   */
  private $created;

  /**
   * @var \DateTime
   */
  private $updated;

  /**
   * @var string
   */
  private $slug;

  /**
   * @var string
   */
  private $image;

  /**
   * @Vich\UploadableField(mapping="images", fileNameProperty="image")
   * @var File
   */
  private $imageFile;

  /**
   * @var string
   */
  private $smallImage;

  /**
   * @Vich\UploadableField(mapping="images", fileNameProperty="smallImage")
   * @var File
   */
  private $smallImageFile;

  /**
   * @var string
   */
  private $largeImage;

  /**
   * @Vich\UploadableField(mapping="images", fileNameProperty="largeImage")
   * @var File
   */
  private $largeImageFile;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   *
   * @return Textpage
   */
  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  /**
   * Get title
   *
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Set lead
   *
   * @param string $lead
   *
   * @return Textpage
   */
  public function setLead($lead)
  {
    $this->lead = $lead;

    return $this;
  }

  /**
   * Get lead
   *
   * @return string
   */
  public function getLead()
  {
    return $this->lead;
  }

  /**
   * Set content
   *
   * @param string $content
   *
   * @return Textpage
   */
  public function setContent($content)
  {
    $this->content = $content;

    return $this;
  }

  /**
   * Get content
   *
   * @return string
   */
  public function getContent()
  {
    return $this->content;
  }

  /**
   * Set highlight
   *
   * @param boolean $highlight
   *
   * @return Textpage
   */
  public function setHighlight($highlight)
  {
    $this->highlight = $highlight;

    return $this;
  }

  /**
   * Get highlight
   *
   * @return boolean
   */
  public function getHighlight()
  {
    return $this->highlight;
  }

  /**
   * Set displayDate
   *
   * @param \DateTime $displayDate
   *
   * @return Textpage
   */
  public function setDisplayDate($displayDate)
  {
    $this->display_date = $displayDate;

    return $this;
  }

  /**
   * Get displayDate
   *
   * @return \DateTime
   */
  public function getDisplayDate()
  {
    return $this->display_date;
  }

  /**
   * Set created
   *
   * @param \DateTime $created
   *
   * @return Textpage
   */
  public function setCreated($created)
  {
    $this->created = $created;

    return $this;
  }

  /**
   * Get created
   *
   * @return \DateTime
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set updated
   *
   * @param \DateTime $updated
   *
   * @return Textpage
   */
  public function setUpdated($updated)
  {
    $this->updated = $updated;

    return $this;
  }

  /**
   * Get updated
   *
   * @return \DateTime
   */
  public function getUpdated()
  {
    return $this->updated;
  }

  /**
   * Set slug
   *
   * @param string $slug
   *
   * @return Textpage
   */
  public function setSlug($slug)
  {
    $this->slug = $slug;

    return $this;
  }

  /**
   * Get slug
   *
   * @return string
   */
  public function getSlug()
  {
    return $this->slug;
  }

  /**
   * Set image
   *
   * @param string $image
   *
   * @return Textpage
   */
  public function setImage($image)
  {
    $this->image = $image;

    return $this;
  }

  /**
   * Get image
   *
   * @return string
   */
  public function getImage()
  {
    return $this->image;
  }

  public function setImageFile(File $image = null)
  {
    $this->imageFile = $image;

    // VERY IMPORTANT:
    // It is required that at least one field changes if you are using Doctrine,
    // otherwise the event listeners won't be called and the file is lost
    if ( $image )
    {
      // if 'updatedAt' is not defined in your entity, use another property
      $this->updatedAt = new \DateTime('now');
    }
  }

  public function getImageFile()
  {
    return $this->imageFile;
  }

  /**
   * Set small image
   *
   * @param string $smallImage
   *
   * @return Textpage
   */
  public function setSmallImage($smallImage)
  {
    $this->smallImage = $smallImage;

    return $this;
  }

  /**
   * Get small image
   *
   * @return string
   */
  public function getSmallImage()
  {
    return $this->smallImage;
  }

  public function setSmallImageFile(File $smallImage = null)
  {
    $this->smallImageFile = $smallImage;

    // VERY IMPORTANT:
    // It is required that at least one field changes if you are using Doctrine,
    // otherwise the event listeners won't be called and the file is lost
    if ( $smallImage )
    {
      // if 'updatedAt' is not defined in your entity, use another property
      $this->updatedAt = new \DateTime('now');
    }
  }

  public function getSmallImageFile()
  {
    return $this->smallImageFile;
  }

  /**
   * Set large image
   *
   * @param string $largeImage
   *
   * @return Textpage
   */
  public function setLargeImage($largeImage)
  {
    $this->largeImage = $largeImage;

    return $this;
  }

  /**
   * Get large image
   *
   * @return string
   */
  public function getLargeImage()
  {
    return $this->largeImage;
  }

  public function setLargeImageFile(File $largeImage = null)
  {
    $this->largeImageFile = $largeImage;

    // VERY IMPORTANT:
    // It is required that at least one field changes if you are using Doctrine,
    // otherwise the event listeners won't be called and the file is lost
    if ( $largeImage )
    {
      // if 'updatedAt' is not defined in your entity, use another property
      $this->updatedAt = new \DateTime('now');
    }
  }

  public function getLargeImageFile()
  {
    return $this->largeImageFile;
  }

  public function getStatus()
  {
    return $this->status;
  }

  public function setStatus($status)
  {
    $this->status = $status;
    return $this;
  }


}
