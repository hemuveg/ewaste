<?php

namespace AppBundle\Twig;

use Twig_Extension;

class AppExtension extends Twig_Extension
{

  public function getFilters()
  {
    return array(
      new \Twig_SimpleFilter('shortenText', array($this, 'shortenText')),
    );
  }

  public function shortenText($text, $wordCount)
  {
    $textArr = explode(' ', $text);
    $ret = array_slice($textArr, 0, $wordCount);

    return implode(' ', $ret) . (count($textArr) > $wordCount ? '...' : '');
  }

  public function getName()
  {
    return 'app_extension';
  }

}
