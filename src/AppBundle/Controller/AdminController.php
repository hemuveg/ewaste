<?php
use Gedmo\Tree\NestedTreeRootRepositoryTest;

namespace AppBundle\Controller;

use AppBundle\Entity\Menuitem;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminController extends BaseAdminController
{
  public function prePersistMenuitemEntity(Menuitem $menuitem)
  {
    $repo = $this->getDoctrine()->getRepository(Menuitem::class);
    $root = $repo->getRootNodesQuery()->getOneOrNullResult();
    if ( !$root )
    {
      $em = $this->get('doctrine.orm.default_entity_manager');
      $root = new Menuitem();
      $root
        ->setTitle('Root')
        ->setSlug('/')
      ;
      $em->persist($root);
      $em->flush();
    }
    $menuitem->setRoot($root);
    if ( !$menuitem->getParent() )
    {
      $menuitem->setParent($root);
    }
  }

  public function MoveUpMenuitemAction()
  {
    $request = $this->request;
    /* @var $repo \Gedmo\Tree\Entity\Repository\NestedTreeRepository */
    $repo = $this->getDoctrine()->getRepository($this->entity['class']);
    $item = $repo->find($request->query->get('id'));
    if ( !$item )
    {
      throw new NotFoundHttpException();
    }
    $repo->moveUp($item);

    return $this->redirect($this->generateUrl('easyadmin', array(
      'action' => 'list',
      'entity' => $this->entity['name'],
      'sortField' => 'lft',
      'sortDirection' => 'ASC',
    )));
  }

  public function MoveDownMenuitemAction()
  {
    $request = $this->request;
    /* @var $repo \Gedmo\Tree\Entity\Repository\NestedTreeRepository */
    $repo = $this->getDoctrine()->getRepository($this->entity['class']);
    $item = $repo->find($request->query->get('id'));
    if ( !$item )
    {
      throw new NotFoundHttpException();
    }
    $repo->moveDown($item);

    return $this->redirect($this->generateUrl('easyadmin', array(
      'action' => 'list',
      'entity' => $this->entity['name'],
      'sortField' => 'lft',
      'sortDirection' => 'ASC',
    )));
  }
}
