<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Menuitem;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{

  public function indexAction(Request $request)
  {
    return $this->render('AppBundle:Default:index.html.twig');
  }

  public function headerAction(Request $request)
  {
    $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Menuitem');
    $menuitems = $repo->findBy(array('lvl' => 1), array('lft' => 'ASC'));
    return $this->render('AppBundle:Default:header.html.twig', array(
      'menuitems' => $menuitems,
    ));
  }

  public function sliderAction()
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    
    $items = $qb->select('p')
      ->from('AppBundle\Entity\Article', 'p')
      ->andWhere($qb->expr()->eq('p.highlight', true))
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->orderBy('p.display_date', 'DESC')
      ->getQuery()->execute(null, Query::HYDRATE_ARRAY);

    return $this->render('AppBundle:Default:slider.html.twig', array(
      'items' => $items,
    ));
  }

  public function footerAction()
  {
    return $this->render('AppBundle:Default:footer.html.twig');
  }

  public function latestAction()
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    
    $items = $qb->select('p')
      ->from('AppBundle\Entity\Article', 'p')
      ->andWhere($qb->expr()->eq('p.highlight', ':highlight'))
        ->setParameter('highlight', false)
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->orderBy('p.display_date', 'DESC')
      ->setMaxResults(4)
      ->getQuery()->execute(null, Query::HYDRATE_ARRAY);

    return $this->render('AppBundle:Default:latest.html.twig', array(
      'items' => $items,
    ));
  }

  public function projectsAction()
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    
    $items = $qb->select('p')
      ->from('AppBundle\Entity\Project', 'p')
      ->andWhere($qb->expr()->eq('p.highlight', ':highlight'))
        ->setParameter('highlight', true)
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->orderBy('p.display_date', 'DESC')
      ->getQuery()->execute(null, Query::HYDRATE_ARRAY);

    return $this->render('AppBundle:Default:projects.html.twig', array(
      'items' => $items,
    ));
  }

  public function blogAction()
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    
    $items = $qb->select('p')
      ->from('AppBundle\Entity\Blogpost', 'p')
      ->andWhere($qb->expr()->eq('p.highlight', ':highlight'))
        ->setParameter('highlight', true)
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->orderBy('p.display_date', 'DESC')
      ->getQuery()->execute(null, Query::HYDRATE_ARRAY);

    return $this->render('AppBundle:Default:blog.html.twig', array(
      'items' => $items,
    ));
  }

  public function contentboxAction(Request $request, $zone, $raw = false)
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    
    $item = $qb->select('p')
      ->from('AppBundle\Entity\Textcontent', 'p')
      ->andWhere($qb->expr()->eq('p.zone', ':zone'))
        ->setParameter('zone', $zone)
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->orderBy('p.display_date', 'DESC')
      ->getQuery()->getOneOrNullResult();

    return $this->render('AppBundle:Default:textcontent.html.twig', array(
      'item' => $item,
      'raw'  => $raw,
    ));
    
  }

  public function pageAction(Request $request, $prefix, $subprefix)
  {
    /* @var $menuitem Menuitem */
    $menuitem = $this->getDoctrine()->getRepository('AppBundle\Entity\Menuitem')->findOneBy(array(
      'slug' => $subprefix ? $subprefix : $prefix,
    ));

    if ( !$menuitem )
    {
      throw new NotFoundHttpException();
    }
    if ( $menuitem->getModule()->getId() == 4 )
    {
      $page = $menuitem->getPage();

      if ( !$page )
      {
        throw new NotFoundHttpException();
      }
      return $this->render('AppBundle:Default:page.html.twig', array(
        'page' => $page,
      ));
    }

    return $this->forward('AppBundle:Default:List', array(
      'type' => $menuitem->getModule()->getTitle(),
      'page' => 1,
    ));
  }

  public function listAction(Request $request, $type, $page)
  {
    switch ($type)
    {
      case 'Blog':
        $entity = 'AppBundle\Entity\Blogpost';
        $route = 'app_blogpost';
        break;
      case 'Articles':
        $entity = 'AppBundle\Entity\Article';
        $route = 'app_newsarticle';
        break;
      case 'Projects':
        $entity = 'AppBundle\Entity\Project';
        $route = 'app_projectarticle';
        break;
      case 'Services':
        $entity = 'AppBundle\Entity\Service';
        $route = 'app_servicearticle';
        break;
    }

    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

    $items = $qb->select('p')
      ->from($entity, 'p')
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->orderBy('p.display_date', 'DESC')
      ->getQuery()->getResult();

    return $this->render('AppBundle:Default:list.html.twig', array(
      'items' => $items,
      'route' => $route,
    ));
  }

  public function showAction(Request $request, $entity, $slug)
  {
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

    $item = $qb->select('p')
      ->from($entity, 'p')
      ->andWhere($qb->expr()->lt('p.display_date', ':now'))
        ->setParameter('now', date('Y-m-d H:i:s'))
      ->andWhere($qb->expr()->eq('p.slug', ':slug'))
        ->setParameter('slug', $slug)
      ->getQuery()->getOneOrNullResult();

    if ( !$item )
    {
      throw new NotFoundHttpException();
    }

    return $this->render('AppBundle:Default:show.html.twig', array(
      'item' => $item,
    ));
  }

}
